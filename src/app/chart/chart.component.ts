import { Component, Input, OnChanges, OnInit } from '@angular/core';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit, OnChanges {


  @Input() data;

  barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
    backgroundColor: '#0da8e0',
    scales: {
      xAxes: [{
        gridLines: {
            display: false
        },
        ticks: {
          fontSize: 30
        }
      }],
      yAxes: [{
        display: true,
        gridLines: {
            display: true
        },
        ticks: {
          beginAtZero: true,
          min: 0,
          max: 1
        }
      }]
    }

 };

  barChartLabels = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
  barChartType = 'bar';
  barChartLegend = false;

  barChartData: any;
  
  ngOnInit(): void {

  }

  ngOnChanges() {
    this.barChartData = [
      {
        data: this.data || [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        label: 'Predictions',
        backgroundColor: '#3e4344',
      }];
  }

}
